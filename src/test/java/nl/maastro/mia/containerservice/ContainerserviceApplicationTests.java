package nl.maastro.mia.containerservice;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.service.ContainerService;
import nl.maastro.mia.containerservice.service.StatusService;
import nl.maastro.mia.containerservice.web.controller.ContainerController;
import nl.maastro.mia.containerservice.web.dto.DicomPackageDto;
import nl.maastro.mia.containerservice.web.dto.ImageDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.util.*;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = ContainerserviceApplication.class)
public class ContainerserviceApplicationTests {
	
    @Autowired
	ContainerController containerController;

	@Autowired
	ContainerService containerService;
	
	@Autowired
	StatusService statusService;
	
	@Test
	public void contextLoads() {
	}
	
    @Test
	public void addDicomPackage(){
		DicomPackageDto dto = new DicomPackageDto();
		dto.setPatientId("patientIdTest");
		dto.setPlanlabel("planLabelTest");
		dto.setRoiNames(new HashSet<String>(Arrays.asList("roi1", "roi2", "roi3")));
		
		ImageDto imageDtoCT = new ImageDto();
		imageDtoCT.setLocation("//ctimage");
		imageDtoCT.setSopUid("ctimage");
		
		ImageDto imageDtoRTSTRUCT = new ImageDto();
		imageDtoRTSTRUCT.setLocation("//rtstruct");
		imageDtoRTSTRUCT.setSopUid("rtstruct");
		
		ImageDto imageDtoRTPLAN = new ImageDto();
		imageDtoRTPLAN.setLocation("//rtplan");
		imageDtoRTPLAN.setSopUid("rtplan");
		
		
		Set<ImageDto> ctList = new HashSet<ImageDto>();
		ctList.add(imageDtoCT);
		
		Map<String, Set<ImageDto>> images = new HashMap<>();
		images.put("CT", ctList);
		
		dto.setImages(images);
		containerController.createContainer(dto, "1", "1", 1);
	}

	@Test
	@Transactional
	public void testDeletionIdle() {
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "IDLE");
		containerController.deleteContainer(container.getId());
		assertTrue(!containerController.getContainer(container.getId()).getStatusCode().is2xxSuccessful());
	}
	
	@Test
	@Transactional
	public void testDeletionConfigurationError(){
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "CONFIGURATIONERROR");
		containerController.deleteContainer(container.getId());
		assertTrue(containerController.getContainer(container.getId()).getStatusCode().is4xxClientError());
	}
	
	@Test
	@Transactional
	public void testDeletionMappingError(){
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "MAPPINGERROR");
		containerController.deleteContainer(container.getId());
		assertTrue(containerController.getContainer(container.getId()).getStatusCode().is4xxClientError());
	}
	
	@Test
	@Transactional
	public void testDeletionCalculationError(){
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "CALCULATIONERROR");
		containerController.deleteContainer(container.getId());
		assertTrue(containerController.getContainer(container.getId()).getStatusCode().is4xxClientError());
	}
	
	@Test
	@Transactional
	public void testDeletionValidationError(){
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "VALIDATIONERROR");
		containerController.deleteContainer(container.getId());
		assertTrue(containerController.getContainer(container.getId()).getStatusCode().is4xxClientError());
	}
	
	@Test
	@Transactional
	public void testDeletionExportError(){
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "EXPORTERROR");
		containerController.deleteContainer(container.getId());
		assertTrue(containerController.getContainer(container.getId()).getStatusCode().is4xxClientError());
	}
	
	@Test
	@Transactional
	public void testDeletionDone(){
		Container container = containerService.createContainer(new DicomPackageDto(), "1", "1", 1);
		statusService.updateContainerStatus(container.getId(), "DONE");
		containerController.deleteContainer(container.getId());
		assertTrue(containerController.getContainer(container.getId()).getStatusCode().is4xxClientError());
	}
	
}