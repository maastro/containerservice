package nl.maastro.mia.containerservice.web.controller;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;
import nl.maastro.mia.containerservice.repository.ContainerRepository;
import nl.maastro.mia.containerservice.service.ContainerService;
import nl.maastro.mia.containerservice.web.dto.ContainerDto;
import nl.maastro.mia.containerservice.web.dto.ContainerIdsDto;
import nl.maastro.mia.containerservice.web.dto.DicomPackageDto;
import nl.maastro.mia.containerservice.web.dto.MappingResultDto;
import nl.maastro.mia.containerservice.web.util.HeaderUtil;
import nl.maastro.mia.containerservice.web.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class ContainerController {

	private final static Logger LOGGER = Logger.getLogger(ContainerController.class);

	@Autowired
	ContainerService containerService;

	@Autowired
	ContainerRepository containerRepository;

	@Value("${spring.application.name:containerservice}")
	String applicationName;

	/**
	 * GET  /containers : get all the container.
	 *
	 * @param pageable the pagination information
	 * @return the ResponseEntity with status 200 (OK) and the list of containers in body
	 * @throws URISyntaxException if there is an error to generate the pagination HTTP headers
	 */
	@RequestMapping(value = "/container",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Container>> getAllContainers(Pageable pageable)
			throws URISyntaxException {
		Page<Container> page = containerRepository.findAll(pageable); 
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/containers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}



	@RequestMapping(value = "/container/user",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContainerIdsDto> getContainersByUserIdInMappingErrorStatus(@RequestBody MappingResultDto mappingResult){

		List<Container> containers = containerRepository.findByUserIdAndContainerStatus(mappingResult.getUserId(), ContainerStatus.MAPPINGERROR);
		List<Long> containerIds = containerService.getContainerIdsReadyForReevaluation(containers, mappingResult.getRtogRoiMap());

		ContainerIdsDto containerIdsDto = new ContainerIdsDto();
		containerIdsDto.setContainerIds(containerIds);

		return Optional.ofNullable(containerIdsDto)
				.map(result -> new ResponseEntity<>(
						result,
						HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}


	/**
	 * SEARCH  /_search/containers?query=:query : search for the container corresponding
	 * to the query.
	 *
	 * @param query the query of the container search
	 * @return the result of the search
	 */
	@RequestMapping(value = "/_search/containers",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Container>> searchContainers(@RequestParam String query, Pageable pageable)
			throws URISyntaxException {
		LOGGER.debug("REST request to search for a page of Containers for query {} " + query);
		Page<Container> page = containerService.search(query, pageable);
		HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/containers");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}


	/**
	 * GET  /container/:id : get the "id" container.
	 *
	 * @param id the id of the container to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the container, or with status 404 (Not Found)
	 */
	@RequestMapping(value = "/container/{id}",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ContainerDto> getContainer(@PathVariable Long id) {
		LOGGER.debug("REST request to get Container : " + id);
		ContainerDto container = containerService.mapContainer(containerRepository.findOne(id));
		return Optional.ofNullable(container)
				.map(result -> new ResponseEntity<>(
						result,
						HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}


	@RequestMapping(value="/container", method=RequestMethod.POST)
	public ResponseEntity<?> createContainer(
			@RequestBody DicomPackageDto dto,
			@RequestParam(required=false) String userId,
			@RequestParam(required=false) String providerId,
			@RequestParam(required=false) Integer inputPort
			){

		Container entity = containerService.createContainer(dto, userId, providerId, inputPort);
		LOGGER.debug("Container created: " + entity);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder
				.fromUriString("http://"+applicationName+"/api/container").path("/{id}")
				.buildAndExpand(entity.getId()).toUri());
		return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
	}


	/**
	 * DELETE  /container/:id : delete the "id" container.
	 *
	 * @param id the id of the container to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@RequestMapping(value = "/container/{id}",
			method = RequestMethod.DELETE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> deleteContainer(@PathVariable Long id) {
		ContainerStatus containerStatus = containerService.getContainerStatus(id);
		LOGGER.debug("REST request to delete Container : {}" + id);
		containerRepository.delete(id);
		return ResponseEntity.ok().headers(
				HeaderUtil.createEntityDeletionAlert("container", id.toString())).build();
	}
}
