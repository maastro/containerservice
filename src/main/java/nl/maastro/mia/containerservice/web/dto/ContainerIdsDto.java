package nl.maastro.mia.containerservice.web.dto;

import java.util.ArrayList;
import java.util.List;

public class ContainerIdsDto {

	private List<Long> containerIds = new ArrayList<>();

	public List<Long> getContainerIds() {
		return containerIds;
	}

	public void setContainerIds(List<Long> containerIds) {
		this.containerIds = containerIds;
	}
}
