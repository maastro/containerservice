package nl.maastro.mia.containerservice.web.dto;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DicomPackageDto {
	String patientId;
	String planLabel;
	Set<String> roiNames;
	Map<String, Set<ImageDto>> images;
	List<String> triggeringSopInstanceUids;

	public DicomPackageDto() {
		roiNames = new HashSet<>();
		images = new HashMap<>();
	}

	public DicomPackageDto(String patientId,
			String planLabel,
			Set<String> roiNames,
			Map<String, Set<ImageDto>> images) {
		super();
		this.patientId = patientId;
		this.planLabel = planLabel;
		this.roiNames = roiNames;
		this.images = images;
	}


	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public String getPlanlabel() {
		return planLabel;
	}

	public void setPlanlabel(String planlabel) {
		this.planLabel = planlabel;
	}

	public Set<String> getRoiNames() {
		return roiNames;
	}

	public void setRoiNames(Set<String> roiNames) {
		this.roiNames = roiNames;
	}

	public Map<String, Set<ImageDto>> getImages() {
		return images;
	}

	public void setImages(Map<String, Set<ImageDto>> images) {
		this.images = images;
	}

	public List<String> getTriggeringSopInstanceUids() {
		return triggeringSopInstanceUids;
	}

	public void setTriggeringSopInstanceUids(List<String> triggeringSopInstanceUids) {
		this.triggeringSopInstanceUids = triggeringSopInstanceUids;
	}
	
}