package nl.maastro.mia.containerservice.web.dto;

import java.util.HashMap;
import java.util.Map;

public class MappingResultDto {

	private String userId;
	private String containerId;
	private Map<String, String> rtogRoiMap = new HashMap<>();
	
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getContainerId() {
		return containerId;
	}
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}
	public Map<String, String> getRtogRoiMap() {
		return rtogRoiMap;
	}
	public void setRtogRoiMap(Map<String, String> rtogRoiMap) {
		this.rtogRoiMap = rtogRoiMap;
	}
}
