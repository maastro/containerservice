package nl.maastro.mia.containerservice.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.service.MappingService;
import nl.maastro.mia.containerservice.web.dto.MappingResultDto;

@RestController
@RequestMapping("/api")
public class MappingController {
	
	@Autowired
	MappingService mappingService;
	
	@RequestMapping(value="/container/mapping", method=RequestMethod.POST)
	public Container addMapping(@RequestBody MappingResultDto mapping){
		return mappingService.addMapping(Long.parseLong(mapping.getContainerId()), mapping.getRtogRoiMap());
	}
	
	@RequestMapping(value="/container/{containerId}/mapping", method=RequestMethod.GET)
	public Map<String, String> getMapping(@PathVariable Long containerId){
		return mappingService.getMapping(containerId);
	}

}
