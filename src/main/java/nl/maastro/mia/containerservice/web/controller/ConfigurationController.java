package nl.maastro.mia.containerservice.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.service.ConfigurationService;

@RestController
@RequestMapping("/api")
public class ConfigurationController {
	
	@Autowired
	ConfigurationService configurationService;

	@RequestMapping(value="/container/{containerId}/configuration", method=RequestMethod.PUT)
	public Container updateInputPort(
			@PathVariable Long containerId, 
			@RequestParam(required=false) Integer inputPort,
			@RequestBody(required=false) String configurationJson
			){
		return configurationService.updateContainer(containerId, inputPort, configurationJson);
	}
	
	@RequestMapping(value="/container/{containerId}/configuration", method=RequestMethod.GET)
	public String getContainerConfiguration(@PathVariable Long containerId){
		return configurationService.getContainerConfiguration(containerId);
	}
	
	@RequestMapping(value="/container/{containerId}/configurationid", method=RequestMethod.PUT)
	public void setContainerConfigurationId(@PathVariable Long containerId, @RequestParam Long configurationId){
		configurationService.setContainerConfigurationId(containerId, configurationId);
	}
	
	@RequestMapping(value="/container/{containerId}/configurationid", method=RequestMethod.GET)
	public Long getContainerConfigurationId(@PathVariable Long containerId){
		return configurationService.getContainerConfigurationId(containerId);
	}
}
