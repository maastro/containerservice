package nl.maastro.mia.containerservice.web.dto;

public class ImageDto {
	private String location;
	private String sopUid;
	
	public ImageDto(){ }
	
	public ImageDto(String location, String sopUid){
		
		this.location = location;
		this.sopUid = sopUid;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSopUid() {
		return sopUid;
	}
	public void setSopUid(String sopUid) {
		this.sopUid = sopUid;
	}
}