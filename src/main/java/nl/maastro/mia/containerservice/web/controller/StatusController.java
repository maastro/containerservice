package nl.maastro.mia.containerservice.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;
import nl.maastro.mia.containerservice.service.StatusService;

@RestController
@RequestMapping("/api")
public class StatusController {
	
	@Autowired
	StatusService statusService;
	
	@RequestMapping(value="/container/{id}/status", method=RequestMethod.PUT)
	public Container putContainerStatus(@PathVariable Long id, @RequestBody String status){
		return statusService.updateContainerStatus(id, status);
	}

	@RequestMapping(value="/container/status/{containerStatus}/{hours}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<PagedResources<Container>> getContainersStatus(
			@PathVariable String containerStatus, 
			@PathVariable Integer hours, 
			Pageable pageable, 
			PagedResourcesAssembler assembler){
		Page<Container> containers = statusService.getContainerStatusAndContainerStatusDate(ContainerStatus.valueOf(containerStatus), hours, pageable);
		return new ResponseEntity<>(assembler.toResource(containers), HttpStatus.OK);
	}
		
	@RequestMapping(value="/container/status/{hours}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<PagedResources<Container>> getContainersStatusDate(@PathVariable Integer hours, Pageable pageable, PagedResourcesAssembler assembler){
		Page<Container> containers = statusService.getContainerStatusDate(hours, pageable);
		return new ResponseEntity<>(assembler.toResource(containers), HttpStatus.OK);
	}

}