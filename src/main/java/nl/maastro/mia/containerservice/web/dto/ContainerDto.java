package nl.maastro.mia.containerservice.web.dto;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;

import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;

public class ContainerDto {
	
    private Long id;
    
    private String userId;
	
    private long configurationId;
    
    private Integer inputPort;
    
	private ContainerStatus containerStatus;
	
	private Date containerStatusDate;
	
	private JsonNode configuration;
	
	private Map<String,String> mappingRtogRoi = new HashMap<>();
	
	private DicomPackageDto dicomPackage;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getConfigurationId() {
        return configurationId;
    }

    public void setConfigurationId(long configurationId) {
        this.configurationId = configurationId;
    }

    public Integer getInputPort() {
        return inputPort;
    }

    public void setInputPort(Integer inputPort) {
        this.inputPort = inputPort;
    }

    public ContainerStatus getContainerStatus() {
        return containerStatus;
    }

    public void setContainerStatus(ContainerStatus containerStatus) {
        this.containerStatus = containerStatus;
    }

    public Date getContainerStatusDate() {
        return containerStatusDate;
    }

    public void setContainerStatusDate(Date containerStatusDate) {
        this.containerStatusDate = containerStatusDate;
    }

    public JsonNode getConfiguration() {
        return configuration;
    }

    public void setConfiguration(JsonNode configuration) {
        this.configuration = configuration;
    }

    public Map<String, String> getMappingRtogRoi() {
        return mappingRtogRoi;
    }

    public void setMappingRtogRoi(Map<String, String> mappingRtogRoi) {
        this.mappingRtogRoi = mappingRtogRoi;
    }

    public DicomPackageDto getDicomPackage() {
        return dicomPackage;
    }

    public void setDicomPackage(DicomPackageDto dicomPackage) {
        this.dicomPackage = dicomPackage;
    }
	
}