package nl.maastro.mia.containerservice.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Service;

import nl.maastro.mia.containerservice.entity.DicomPackageEntity;
import nl.maastro.mia.containerservice.entity.ImageEntity;
import nl.maastro.mia.containerservice.entity.ModalityEntity;
import nl.maastro.mia.containerservice.web.dto.DicomPackageDto;
import nl.maastro.mia.containerservice.web.dto.ImageDto;

@Service
public class DicomPackageService {
	
	public DicomPackageDto mapDicomPackage(DicomPackageEntity entity){
		DicomPackageDto dto = new DicomPackageDto();
		dto.setPatientId(entity.getPatientId());
		dto.setPlanlabel(entity.getPlanLabel());
		dto.setRoiNames(entity.getRoiNames());
		dto.setImages(mapImages(entity.getModalities()));
		dto.setTriggeringSopInstanceUids(entity.getTriggeringSopInstanceUids());
		return dto;
	}
	
	public DicomPackageEntity mapDicomPackage(DicomPackageDto dto){
		DicomPackageEntity entity = new DicomPackageEntity();
		entity.setPatientId(dto.getPatientId());
		entity.setPlanLabel(dto.getPlanlabel());
		entity.setRoiNames(dto.getRoiNames());
		entity.setTriggeringSopInstanceUids(dto.getTriggeringSopInstanceUids());
		if(dto.getImages()!=null)
			entity.setModalities(mapImagesMap(dto.getImages()));
		return entity;
	}
	
	private Map<String, Set<ImageDto>> mapImages(Set<ModalityEntity> modalities){
		Map<String, Set<ImageDto>> imagesMap = new HashMap<>();
		for(ModalityEntity modality : modalities){
			Set<ImageDto> dtoSet = new HashSet<>();
			for(ImageEntity image : modality.getImages()){
				dtoSet.add(mapImage(image));
			}
			imagesMap.put(modality.getModality(), dtoSet);
		}
		return imagesMap;
	}
	
	private Set<ModalityEntity> mapImagesMap(Map<String, Set<ImageDto>> images){
		
		Set<ModalityEntity> modalities = new HashSet<>();
		
		for(String key : images.keySet()){
			
			Set<ImageEntity> imageEntities = new HashSet<>();
			for(ImageDto imageDto : images.get(key)){
				imageEntities.add(mapImage(imageDto));
			}
			
			ModalityEntity entity = new ModalityEntity();
			entity.setModality(key);
			entity.setImages(imageEntities);
			modalities.add(entity);
		}
		return modalities;	
	}
	
	
	
	private ImageDto mapImage(ImageEntity entity){
		ImageDto dto = new ImageDto();
		dto.setLocation(entity.getLocation());
		dto.setSopUid(entity.getSopUid());
		return dto;
	}
	
	private ImageEntity mapImage(ImageDto dto){
		ImageEntity entity = new ImageEntity();
		entity.setLocation(dto.getLocation());
		entity.setSopUid(dto.getSopUid());
		return entity;
	}
}
