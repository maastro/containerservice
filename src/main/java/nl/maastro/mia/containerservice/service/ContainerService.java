package nl.maastro.mia.containerservice.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;
import nl.maastro.mia.containerservice.repository.ContainerRepository;
import nl.maastro.mia.containerservice.web.dto.ContainerDto;
import nl.maastro.mia.containerservice.web.dto.DicomPackageDto;

@Service
public class ContainerService {
	
	private static final Logger LOGGER = Logger.getLogger(ContainerService.class);
	private static final String NOT_PRESENT = "NotPresent";
	
	@Autowired
	private ContainerRepository containerRepository;
	
	@Autowired
	private DicomPackageService dicomPackageService;
			
	public Container createContainer(DicomPackageDto dto, String userId, String providerId, Integer inputPort){
		
		LOGGER.debug("Received dicomPackageDto: " +dto+ " userId:"+userId);
		if(dto==null){
			LOGGER.warn("received empty dicomPackageDto post");
			return null;
		}
	
		Container entity = new Container();
		entity.setContainerStatus(ContainerStatus.IDLE);
		entity.setContainerStatusDate(new Date());
		entity.setDicomPackageEntity(dicomPackageService.mapDicomPackage(dto));
		entity.setUserId(userId);
		entity.setProviderId(providerId);
		entity.setInputPort(inputPort);
		entity.setPatientId(dto.getPatientId());
		return containerRepository.save(entity);
	}
	
	public ContainerDto mapContainer(Container entity){
		if(entity==null)
			return null;
		ContainerDto dto = new ContainerDto();
		dto.setId(entity.getId());
		dto.setUserId(entity.getUserId());
		dto.setInputPort(entity.getInputPort());
		dto.setMappingRtogRoi(entity.getMappingRtogRoi());
		if(entity.getConfigurationJson()!=null){
			ObjectMapper mapper = new ObjectMapper();
			JsonNode result;
			try {
				result = mapper.readTree(entity.getConfigurationJson());
				dto.setConfiguration(result);

			} catch (IOException e) {
				LOGGER.error("Could not read tree: " + entity.getConfigurationJson(), e);
			}
			
		}
		if(null!=entity.getConfigurationId())
			dto.setConfigurationId(entity.getConfigurationId());
		dto.setContainerStatus(entity.getContainerStatus());
		dto.setContainerStatusDate(entity.getContainerStatusDate());
		dto.setDicomPackage(dicomPackageService.mapDicomPackage(entity.getDicomPackageEntity()));
		return dto;
	}
	
	public Page<Container> search(String query, Pageable pageable){
		
		ContainerStatus status = this.matchContainerStatus(query);
		if (status != null){
			return containerRepository.findByContainerStatus(status, pageable);
		}
		  try{
		        Integer inputPort = Integer.parseInt(query);
		        return containerRepository.findByUserIdOrPatientIdOrInputPort(query, query, inputPort, pageable);
		    }catch(NumberFormatException e){
		    	return containerRepository.findByUserIdOrPatientId(query, query, pageable);
		    }
	}
	
	private ContainerStatus matchContainerStatus(String query){
		for (ContainerStatus status : ContainerStatus.values()){
			if (status.name().equalsIgnoreCase(query)){
				return status;
			}
		}
		return null;
	}
	
	public List<Long> getContainerIdsReadyForReevaluation(List<Container> containers, Map<String, String> mappings){
		List<Long> containerIds = new ArrayList<>();
		
		Map<String, String> viableMappings = new HashMap<>();
		for (Entry<String, String> mapping : mappings.entrySet()){
			if (!StringUtils.isBlank(mapping.getValue()) && !NOT_PRESENT.equalsIgnoreCase(mapping.getValue())){
				viableMappings.put(mapping.getKey(), mapping.getValue());
			}
		}
		
		if (viableMappings.isEmpty()){
			LOGGER.info("No viable mappings found: " + mappings.toString() + ". Returning empty list");
			return Collections.emptyList();
		}
		
		for (Container container : containers){
			if (this.mapped(container, viableMappings)){
				containerIds.add(container.getId());
			}
		}
		
		return containerIds;
	}
	
	/**
	 * Gets the status of the container with the supplied id.
	 * @param id Id of the Container to get the status for.
	 * @return The status of the container, or null if the container doesn't exist.
	 */
	public ContainerStatus getContainerStatus(Long id){
		Container container = containerRepository.findOne(id);
		if(container != null){
			return container.getContainerStatus();
		}else{
			return null;
		}
	}
	
	private boolean mapped(Container container, Map<String, String> mappings){
		if (container.getDicomPackageEntity() == null 
			|| container.getDicomPackageEntity().getRoiNames() == null
			|| container.getMappingRtogRoi() == null){
			return false;
		}
		Set<String> roiNames = container.getDicomPackageEntity().getRoiNames();

		for (Entry<String, String> rtogRoi : container.getMappingRtogRoi().entrySet()){
			if (StringUtils.isBlank(rtogRoi.getValue()) && !roiNames.contains(mappings.get(rtogRoi.getKey()))){
				return false;
			}
		}
		return true;
	}
	
}