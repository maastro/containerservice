package nl.maastro.mia.containerservice.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.repository.ContainerRepository;

@Service
public class MappingService {
		
	@Autowired
	ContainerRepository containerRepository;
		
	public Container addMapping(Long containerId, Map<String, String> mapping){
		Container containerEntity = containerRepository.findOne(containerId);
		if(containerEntity==null)
			return null;
		containerEntity.setMappingRtogRoi(mapping);
		return containerRepository.save(containerEntity);
	}

	public Map<String, String> getMapping(Long containerId) {
		Container containerEntity = containerRepository.findOne(containerId);
		if(containerEntity==null || containerEntity.getMappingRtogRoi()==null)
			return null;
		else
			return containerEntity.getMappingRtogRoi();
	}

}
