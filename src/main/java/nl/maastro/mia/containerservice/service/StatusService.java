package nl.maastro.mia.containerservice.service;

import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;
import nl.maastro.mia.containerservice.repository.ContainerRepository;

@Service
public class StatusService {
	
	private static final Logger LOGGER = Logger.getLogger(StatusService.class);

	@Autowired
	ContainerRepository containerRepository;
	
	/**
	 * Get all containers with 
	 * @param containerStatus the status of the container
	 * @param hours the minimum age if the container in hours
	 * @return
	 */
	public Page<Container>getContainerStatusAndContainerStatusDate(ContainerStatus containerStatus, int hours, Pageable pageable){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR, -hours);
		return containerRepository.findByContainerStatusAndContainerStatusDateBefore(containerStatus, c.getTime(), pageable);
	}
	
	/**
	 * Get all containers with 
	 * @param hours the minimum age if the container in hours
	 * @return
	 */
	public Page<Container>getContainerStatusDate(int hours, Pageable pageable){
		Calendar c = Calendar.getInstance();
		c.add(Calendar.HOUR, -hours);
		return containerRepository.findByContainerStatusDateBefore(c.getTime(), pageable);
	}
	
	public Container updateContainerStatus(Long id, String status) {
		ContainerStatus containerStatus = ContainerStatus.valueOf(status);
		Container entity = containerRepository.getOne(id);
		entity.setContainerStatus(containerStatus);
		entity.setContainerStatusDate(new Date());
		LOGGER.info("Status updated containerId:" + entity.getId() + ", status:" + status);
		return containerRepository.save(entity);
	}
}
 