package nl.maastro.mia.containerservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.repository.ContainerRepository;

@Service
public class ConfigurationService {

	@Autowired
	ContainerRepository containerRepository;
	
	public Container updateContainer(Long containerId, Integer inputPort, String configurationJson){
		Container container = containerRepository.findOne(containerId);
		if(container==null)
			return null;
		else{
			if(configurationJson!=null)
				container.setConfigurationJson(configurationJson);
			if(inputPort!=null)
				container.setInputPort(inputPort);
			return containerRepository.save(container);
		}	
	}
	
	public String getContainerConfiguration(Long containerId){
		return containerRepository.getOne(containerId).getConfigurationJson();
	}

	public void setContainerConfigurationId(Long containerId, Long configurationId) {
		Container container = containerRepository.getOne(containerId);
		container.setConfigurationId(configurationId);
		containerRepository.save(container);
	}

	public Long getContainerConfigurationId(Long containerId) {
		return containerRepository.getOne(containerId).getConfigurationId();
	}
	
}
