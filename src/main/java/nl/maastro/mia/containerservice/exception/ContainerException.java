package nl.maastro.mia.containerservice.exception;

public class ContainerException extends Exception{

	private static final long serialVersionUID = 3758217712874054512L;

	public ContainerException(){
		super();
	}
	
	public ContainerException(String message){
		super(message);
	}
	
	public ContainerException(Throwable cause){
		super(cause);
	}
	
	public ContainerException(String message, Throwable cause){
		super(message, cause);
	}
}
