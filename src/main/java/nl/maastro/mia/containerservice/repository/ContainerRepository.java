package nl.maastro.mia.containerservice.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;

@Repository
public interface ContainerRepository extends JpaRepository<Container, Long>{
	
    List<Container> findByContainerStatusDateBefore(Date containerStatusDate);
    
	Page<Container> findByContainerStatusDateBefore(Date containerStatusDate, Pageable pageable);

	Page<Container> findByContainerStatusAndContainerStatusDateBefore(ContainerStatus containerStatus, Date containerStatusDate, Pageable pageable);
	
	Page<Container> findByContainerStatus(ContainerStatus containerStatus, Pageable pageable);
	
	Page<Container> findByUserIdOrPatientIdOrInputPort(String userId, String patientId, Integer inputPort, Pageable pageable);
	
	List<Container> findByUserIdAndContainerStatus(String userId, ContainerStatus containerStatus);

	Page<Container> findByUserIdOrPatientId(String userId, String patientId, Pageable pageable);
	
}
