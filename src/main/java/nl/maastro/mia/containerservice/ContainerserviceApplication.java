package nl.maastro.mia.containerservice;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ContainerserviceApplication {

	private static final Logger LOGGER = Logger.getLogger(ContainerserviceApplication.class);
	private static final String name = ContainerserviceApplication.class.getPackage().getName();
	private static final String version = ContainerserviceApplication.class.getPackage().getImplementationVersion();

	public static void main(String[] args) {
		SpringApplication.run(ContainerserviceApplication.class, args);
		LOGGER.info("**** VERSION **** : " + name + " " +  version );
	}
}
