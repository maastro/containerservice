package nl.maastro.mia.containerservice.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;

@Entity
public class DicomPackageEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String patientId;	
	private String planLabel;
		
	@ElementCollection
	private Set<String> roiNames;
	@ElementCollection
	private List<String> triggeringSopInstanceUids;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
   	@PrimaryKeyJoinColumn
	private Set<ModalityEntity> modalities = new HashSet<>();
	
	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}

	public Set<String> getRoiNames() {
		return roiNames;
	}

	public void setRoiNames(Set<String> roiNames) {
		this.roiNames = roiNames;
	}

	public String getPlanLabel() {
		return planLabel;
	}

	public void setPlanLabel(String planLabel) {
		this.planLabel = planLabel;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Set<ModalityEntity> getModalities() {
		return modalities;
	}

	public void setModalities(Set<ModalityEntity> modalities) {
		this.modalities = modalities;
	}
	

	public List<String> getTriggeringSopInstanceUids() {
		return triggeringSopInstanceUids;
	}

	public void setTriggeringSopInstanceUids(List<String> triggeringSopInstanceUids) {
		this.triggeringSopInstanceUids = triggeringSopInstanceUids;
	}

	@Override
	public String toString() {
		return "DicomPackageEntity [id=" + id + ", patientId=" + patientId + ", planLabel=" + planLabel
				+ ", roiNames=" + roiNames + ", triggeringSopUids=" + triggeringSopInstanceUids + ", modalities=" + modalities
				+ "]";
	}


	
}