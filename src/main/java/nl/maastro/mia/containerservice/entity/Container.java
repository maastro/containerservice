package nl.maastro.mia.containerservice.entity;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import nl.maastro.mia.containerservice.entity.enumeration.ContainerStatus;

@Entity
public class Container {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private ContainerStatus containerStatus;	
	private Date containerStatusDate;
	
	private String patientId;
	private String userId;	
	private String providerId;
	private Integer inputPort;
	private Long configurationId;
	
	@Column(columnDefinition="text")
	private String configurationJson;
		
	@OneToOne(cascade = CascadeType.ALL, optional = false, fetch = FetchType.EAGER, orphanRemoval = true)
    @JoinColumn(name="package_id")
	private DicomPackageEntity dicomPackageEntity;
		
	@ElementCollection
	private Map<String,String> mappingRtogRoi = new HashMap<>();
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public ContainerStatus getContainerStatus() {
		return containerStatus;
	}
	public void setContainerStatus(ContainerStatus containerStatus) {
		this.containerStatus = containerStatus;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public DicomPackageEntity getDicomPackageEntity() {
		return dicomPackageEntity;
	}
	public void setDicomPackageEntity(DicomPackageEntity dicomPackageEntity) {
		this.dicomPackageEntity = dicomPackageEntity;
	}
	public Map<String, String> getMappingRtogRoi() {
		return mappingRtogRoi;
	}
	public void setMappingRtogRoi(Map<String, String> mappingRtogRoi) {
		this.mappingRtogRoi = mappingRtogRoi;
	}
	public Date getContainerStatusDate() {
		return containerStatusDate;
	}
	public void setContainerStatusDate(Date containerStatusDate) {
		this.containerStatusDate = containerStatusDate;
	}
	public String getProviderId() {
		return providerId;
	}
	public void setProviderId(String providerId) {
		this.providerId = providerId;
	}
	public String getConfigurationJson() {
		return configurationJson;
	}
	public void setConfigurationJson(String configurationJson) {
		this.configurationJson = configurationJson;
	}
	@Override
	public String toString() {
		return "ContainerEntity [id=" + id + ", containerStatus=" + containerStatus + ", containerStatusDate="
				+ containerStatusDate + ", userId=" + userId + ", providerId=" + providerId + ", inputPort="
				+ inputPort + ", dicomPackageEntity=" + dicomPackageEntity
				+ ", mappingRtogRoi=" + mappingRtogRoi + "]";
	}
	public String getPatientId() {
		return patientId;
	}
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	public Integer getInputPort() {
		return inputPort;
	}
	public void setInputPort(Integer inputPort) {
		this.inputPort = inputPort;
	}
	public Long getConfigurationId() {
		return configurationId;
	}
	public void setConfigurationId(Long configurationId) {
		this.configurationId = configurationId;
	}

	
}
