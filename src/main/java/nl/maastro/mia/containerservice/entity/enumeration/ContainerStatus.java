package nl.maastro.mia.containerservice.entity.enumeration;

/**
 * The CalculationStatus enumeration.
 */
public enum ContainerStatus {
    IDLE,PREPROCESSING,QUEUE,RUNNING,DONE,CONFIGURATIONERROR,MAPPINGERROR,VALIDATIONERROR,CALCULATIONERROR,EXPORTERROR,CONNECTIONERROR,REQUEUE,RERUNNING
}
