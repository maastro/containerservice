package nl.maastro.mia.containerservice.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ImageEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String location;
	private String sopUid;
		
	public ImageEntity(){ }
	
	public ImageEntity(String location, String sopUid){
		
		this.location = location;
		this.sopUid = sopUid;
	}
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSopUid() {
		return sopUid;
	}
	public void setSopUid(String sopUid) {
		this.sopUid = sopUid;
	}

	@Override
	public String toString() {
		return "ImageEntity [id=" + id + ", location=" + location + ", sopUid=" + sopUid + "]";
	}
	
}