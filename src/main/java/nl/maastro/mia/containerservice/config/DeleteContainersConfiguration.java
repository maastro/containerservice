package nl.maastro.mia.containerservice.config;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import nl.maastro.mia.containerservice.entity.Container;
import nl.maastro.mia.containerservice.repository.ContainerRepository;

@Component
@EnableScheduling
@ConfigurationProperties(prefix="scheduled-task.delete-containers")
public class DeleteContainersConfiguration {
    
    private static final Logger logger = Logger.getLogger(DeleteContainersConfiguration.class);
    
    private int deleteAfterXDays = 7;
    
    private ContainerRepository containerRepository;
    
    public DeleteContainersConfiguration(ContainerRepository containerRepository) {
        this.containerRepository = containerRepository;
    }
    
    @Scheduled(cron="${scheduled-task.delete-containers.cron:0 0 0 * * *}")
    public void deleteContainersTask() {
        logger.info("Scheduled task to delete all containers with last status update older than " + deleteAfterXDays + " days");
        Instant nowMinusXDays = Instant.now().minus(deleteAfterXDays, ChronoUnit.DAYS);
        List<Container> containersToDelete = containerRepository.findByContainerStatusDateBefore(Date.from(nowMinusXDays));
        for (Container container : containersToDelete) {
            containerRepository.delete(container);
            logger.info("Deleted container with id: " + container.getId());
        }
    }

    public int getDeleteAfterXDays() {
        return deleteAfterXDays;
    }

    public void setDeleteAfterXDays(int deleteAfterXDays) {
        this.deleteAfterXDays = deleteAfterXDays;
    }
}
