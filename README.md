# MIA Container Service
 
The ContainerService keeps stores all containers in the [Medical Image Analysis framework](https://bitbucket.org/maastrosdt/binaries-and-documentation/wiki/Home). A container is a computation entity containing both the computation configuration and the required DICOM files for a given computation. 

## Prerequisites ##

- Java 8
- 512 GB RAM
- Modern browser 

## Container Contents ##

A container keeps track of all information relevant for a single computation run:

- **Dicom data**: All referenced DICOM information relevant for this computation.
- **Configuration**: JSON with the configuration for all computations configured for this run. Stored by configurationid.
- **Mappings**: Mappings of Region of Interests in the RTSTRUCT to their RTOG convention synonyms. 
- **Status**: Current calculation status of the container:
    - *IDLE*: Container has not yet been queued and is awaiting further assessment. 
    - *MAPPINGERROR*: Some of the ROIs in the RTSTRUCT required for computations have no known RTOG conform synonym
    - *QUEUED*: Container is ready for calculations, but all workers are currently busy.
    - *CONFIGURATIONERROR*: A problem has occurred while attempting to add configuration to the container.
    - *CALCULATIONERROR*: Calculations were started for the container. During the calculations, something went wrong.
    - *CONNECTIONERROR*: One of the other microservices could not be reached while attempting calculations.
    - *EXPORTERROR*: Calculations finished, but something went wrong publishing the results.
    - *DONE*: Calculations were finished and results were exported successfully.

## Functional Log ##

In addition to database tables containing the container data, there is a table named 'Container_Log' which contains the functional log

**Database table**

The following table describes the column names and their description:

| Column name   |                                             Description                                            |
|---------------|:--------------------------------------------------------------------------------------------------:|
| Level         | The logging level as a string i.e. DEBUG, INFO, ERROR, ...                                         |
| LevelInteger  | The logging level as an integer. ALL(0),TRACE(1),DEBUG(2),INFO(3),WARN(4),ERROR(5),FATAL(6),OFF(7) |
| EntryDate     | The date at which the log was added. yyy-mm-dd hh:mm:ss.SSS                                        |
| ContainerId   | Id of the container on which this log is based                                                     |
| PatientId     | Patient's ID                                                                                       |
| PlanLabel     | Plan label                                                                                         |
| Fraction      | Fraction. Only used in some DGRT modules. Is null or empty otherwise                               |
| Status        |  Status of the container i.e. PROCESSING, RUNNING, CALCULATIONERROR, ...                           |
| CalculationId | ID of the calculation. Only available while running in worker service                              |
| ServiceName   | Name of the service from which this log originates                                                 |
| MessageUuid   | Unique ID which references to a message. Used for grouping messages from the same source           |
| Message       | The actual log message                                                                             |

## Usage ##

 The [swagger interface](http://localhost:8200/swagger-ui.html) will provide a summary of all possible functions and requests.